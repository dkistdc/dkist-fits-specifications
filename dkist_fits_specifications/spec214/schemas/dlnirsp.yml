spec214:
  section_122: dlnirsp
  copy: true
  required: false
  units: null
  expand: false
  title: DL-NIRSP Instrument
  summary: Keys specific to the operation of the DL-NIRSP instrument.
---
DLARMID:
  rename: "DLN__001"
  description: "Identifier for DL-NIRSP arm that generated the data"
DLARMPS:
  rename: "DLN__002"
  description: "Position of the arm linear stage."
  units: mm
DLARMFC:
  rename: "DLN__003"
  description: "Position of the camera focus stage."
  units: mm
DLFILT:
  rename: "DLN__004"
  description: "Unique ID of the filter in use"
DLWAV:
  rename: "DLN__005"
  description: "Central wavelength of the filter in use"
  units: nm
DLFWHM:
  rename: "DLN__006"
  description: "Full Width at Half Maximum spectral transmission at the current filter in use"
  units: nm
DLFLTPOS:
  rename: "DLN__007"
  description: "Position of the filter wheel rotation stage"
  units: deg
DLPOLMD:
  rename: "DLN__008"
  description: "Polarimeter Mode with which these data were acquired"
DLMODID:
  rename: "DLN__009"
  description: "Unique identifier of the modulator."
DLMOD:
  rename: "DLN__010"
  description: "The type of motion used by the modulator."
DLMODRT:
  rename: "DLN__011"
  description: "Rate at which modulation states are acquired."
  units: Hz
DLMODN:
  rename: "DLN__012"
  description: "Integer number identifier corresponding to the particular state defined by the position of the modulator at its reference time"
  level0_only: true
DLMDANG:
  rename: "DLN__013"
  description: "Modulator angle corresponding to the modulation state at the reference time."
  units: deg
  level0_only: true
DLNUMST:
  rename: "DLN__014"
  description: "Number of states to be acquired during a modulation cycle:
                NumberOfStates ≥ 1. It is required that there be a one-to-one sequential mapping from modulator states to CSS accumulators."
DLSTNUM:
  rename: "DLN__015"
  description: "Number of the current modulation state:
                1 ≤ CurrentStateNumber ≤ NumberOfStates"
  level0_only: true
DLGRTID:
  rename: "DLN__016"
  description: "Unique identifier of the grating in use."
DLGRTCN:
  rename: "DLN__017"
  description: "Grating constant, in lines per mm."
  units: 1/mm
DLGRTBA:
  rename: "DLN__018"
  description: "Grating blaze angle."
  units: deg
DLGRTAN:
  rename: "DLN__019"
  description: "Grating angle with respect to incident beam."
  units: deg
DLNDATAC:
  rename: "DLN__020"
  description: "The number of data cycles that should occur for each mosaic tile. A data cycle can be a single or multiple modulation states. A modulation cycle is defined in discrete sampling by the collection of state values. In continuous sampling, it is defined as one half-rotation of the modulator."
DLCURDAT:
  rename: "DLN__021"
  description: "The index of the current data cycle at this mosaic tile position."
  level0_only: true
DLCOADD:
  rename: "DLN__022"
  description: "Defines how data were coadded by the detector. Values are defined as follows:
                State: All coadded exposures for a single state are acquired prior to progressing to the next modulation state (discrete modulation). Equivalent states are coadded.
                Sequence: The full modulation sequence for a half rotation is cycled through a number of times equivalent to the number of accumulations.
                Half-rotation: States of the first half rotation are coadded with comparable states of the second half rotation.
                None: All exposures are saved without coadding."
DLFRATIO:
  rename: "DLN__023"
  description: "Configuration of the DL-NIRSP Feed Optics"
DLF4POS:
  rename: "DLN__024"
  description: "Position of the Feed Optics fold mirror 4 stage which selects the focal ratio."
  units: deg
DLF1POS:
  rename: "DLN__025"
  description: "Position of the Feed Optics f/62 mirror 1 stage which selects the focal ratio."
  units: mm
DLIFU:
  rename: "DLN__026"
  description: "Value indicating IFU that was used for this observation"
DLIFUPOS:
  rename: "DLN__027"
  description: "Position of the IFU stage"
  units: mm
DLSLITMD:
  rename: "DLN__028"
  description: "Value indicating whether DL-NIRSP was operated in single, multi-slit, or dark configuration"
DLSLIT:
  rename: "DLN__029"
  description: "Identifier of the slit used when DL-NIRSP is operated in single slit mode.
                Keyword not present, if not used"
DLSLITPS:
  rename: "DLN__030"
  description: "Position of the slit mask stage"
  units: mm
DLMOSNRP:
  rename: "DLN__031"
  description: "Total number of times the mosaic pattern is to repeat."
DLCURMOS:
  rename: "DLN__032"
  description: "Index of the current repeat of the mosaic."
DLMSPAT:
  rename: "DLN__033"
  description: "The type of scanning pattern used to make the mosaic, e.g. 'left-right zig-zag"
DLNSSTPX:
  rename: "DLN__034"
  description: "Number of mosaic steps in horizontal direction belonging to the scanned FieldOfView.
                Keyword not present, if not used"
DLSTPCPX:
  rename: "DLN__035"
  description: "Angular position of the center position in the horizontal direction of the field scan given in units of the field scanning mirror motorized stage. Default position will be a property updated when the instrument is co-aligned with other instruments using a GOS target."
  units: urad
DLSTPX:
  rename: "DLN__036"
  description: "Angular step size in the horizontal direction given in units of the field scanning mirror motorized stage. Default value will be calculated according to IFU, slits, and feed optics selection."
  units: urad
DLCSTPX:
  rename: "DLN__037"
  description: "Current image position in horizontal direction belonging to the scanned FieldOfView.
                1 ≤ CurrentSpatialStepX ≤ NumberofSpatialStepsX
                Keyword not present, if not used"
DLNSSTPY:
  rename: "DLN__038"
  description: "Value indicating the number of images in vertical direction belonging to the scanned FieldOfView.
                Keyword not present, if not used"
DLSTPCPY:
  rename: "DLN__039"
  description: "Angular position of the center position in the vertical direction of the field scan given in units of the field scanning mirror motorized stage. Default position will be a property updated when the instrument is co-aligned with other instruments using a GOS target."
  units: urad
DLSTPY:
  rename: "DLN__040"
  description: "Angular step size in the vertical direction given in units of the field scanning mirror motorized stage. Default value will be calculated according to IFU, slits, and feed optics selection."
  units: urad
DLCSTPY:
  rename: "DLN__041"
  description: "Value indicating the current image position in vertical direction belonging to the scanned FieldOfView.
                1 ≤ CurrentSpatialStepY ≤ NumberOfSpatialStepsY
                Keyword not present, if not used"
DLDMODE:
  rename: "DLN__042"
  description: "Boolean indicating whether the DL-NIRSP dithering mode was activated or not. When enabled, a sub-unit field shift is used between successive completed FieldSamples"
DLDOFFX:
  rename: "DLN__043"
  description: "Offset of the dithered position in the X direction. Keyword not present if DitherMode is false."
  units: urad
DLDOFFY:
  rename: "DLN__044"
  description: "Offset of the dithered position in the Y direction. Keyword not present if DitherMode is false."
  units: urad
DLCURSTP:
  rename: "DLN__045"
  description: "A boolean to indicate whether this spatial step corresponds to the dithered position. Keyword not present if DitherMode is false."
DLCAMSMD:
  rename: "DLN__046"
  description: "A keyword for H2RGs only. Distinguishes whether the camera was operating in sub- frame time integration."
DLCAMSSQ:
  rename: "DLN__047"
  description: "A keyword for H2RGs only. Gives the pattern of reset and read frames, e.g. '1 line 2 read'"
DLCAMNSF:
  rename: "DLN__048"
  description: "A Keyword for H2RGs only. The number of science frames acquired per ramp, in the '1 line 2 read' example, the number of science frames would be 2."
DLCAMNS:
  rename: "DLN__049"
  description: "A keyword for H2RGs only. The total number of frames in a read sequence, including bias or calibration frames, acquired per ramp."
DLCAMCUR:
  rename: "DLN__050"
  description: "A keyword for H2RGs only. The index of the sample in a read sequence."
  level0_only: true
DLCAMOFF:
  rename: "DLN__051"
  description: "A keyword for H2RGs only. How long (average) after the trigger does the exposure start"
  units: ms
DLCAMTIM:
  rename: "DLN__052"
  description: "A keyword for H2RGs only. How long (average) between exposure starts"
  units: ms
DLARTEMP:
  rename: "DLN__053"
  description: "A keyword for H2RGs only. Most recent temperature of the detector array in Kelvin."
  units: K
DLCAMFID:
  rename: "DLN__054"
  description: "A keyword for H2RGs only. Identifier for the camera cold filter or prefilter."
DLMODT0:
  rename: "DLN__055"
  description: "UTC Data/Time of the modulator reference time, T0, in the form:
                YYYY-MM-DDThh:mm:ss.sss[…]
                Accuracy at minimum to [ms]"
