.. _api-reference:

``dkist_fits_specifications`` Package
-------------------------------------

.. automodapi:: dkist_fits_specifications
   :no-heading:

.. automodapi:: dkist_fits_specifications.spec122
   :headings: #^

.. automodapi:: dkist_fits_specifications.spec214
   :headings: #^

.. automodapi:: dkist_fits_specifications.utils
   :headings: #^
   :include-all-objects:
