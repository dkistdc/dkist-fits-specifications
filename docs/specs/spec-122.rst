.. _spec-122:

Level Zero FITS Specification
=============================

The level 0 specification for FITS files is reproduced here for convenience.

The level 0 data archived and made available through the DKIST Data Center will also have some keys from 214 added to it to provide some level of standardisation between level 0 and level 1.


Level Zero Header Specification
-------------------------------

FITS Keywords
#############

.. spec-122-table:: fits

Telescope Keywords
##################

.. spec-122-table:: telescope

DKIST ID Keywords
#################

.. spec-122-table:: dkist-id

DKIST Operational Keywords
##########################

.. spec-122-table:: dkist-dkist

Camera Keywords
###############

.. spec-122-table:: camera

PA&C Keywords
#############

.. spec-122-table:: pac

Adaptive Optics Keywords
########################

.. spec-122-table:: ao

Weather Station Keywords
########################

.. spec-122-table:: ws

VBI Instrument Keywords
#######################

.. spec-122-table:: vbi

VISP Instrument Keywords
########################

.. spec-122-table:: visp

Cryo-NIRSP Instrument Keywords
##############################

.. spec-122-table:: cryonirsp

DL-NIRSP Instrument Keywords
############################

.. spec-122-table:: dlnirsp

VTF Instrument Keywords
#######################

.. spec-122-table:: vtf

Wavefront Correction System Keywords
####################################

.. spec-122-table:: wfc
